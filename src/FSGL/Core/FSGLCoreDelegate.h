#ifndef FSFGLCORERENDERERDELEGATE_H
#define FSFGLCORERENDERERDELEGATE_H

class FSGLCore;

namespace FSGL {

class FSGLCoreDelegate {

public:
	virtual void coreDidTapButton(shared_ptr<FSGLCore> core, shared_ptr<Button> button) = 0;

};
};

#endif