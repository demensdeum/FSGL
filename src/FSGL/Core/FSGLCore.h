/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   FSEOGLCore.h
 * Author: demensdeum
 *
 * Created on July 8, 2017, 10:10 AM
 */

#ifndef FSGLCORE_H
#define FSGLCORE_H

#include <string>
#include <memory>
#include <map>

#include <FlameSteelCommonTraits/Screenshot.h>
#include <FSGL/Data/SystemParams.h>
#include <FSGL/Renderer/Renderer.h>
#include <FSGL/Data/Button/Button.h>
#include <FSGL/Core/FSGLCoreDelegate.h>

struct SDL_Window;
class FSGLObject;
class FSGLCamera;

using namespace FSGL;
using namespace std;

namespace FSGL
{
class Renderer;
};

using namespace FSGL;

class FSGLCore: public RendererDelegate, public enable_shared_from_this<FSGLCore>
{
public:
    FSGLCore();
    virtual ~FSGLCore();

	weak_ptr<FSGLCoreDelegate> delegate;

    void preInitialize();
    void fillParams(shared_ptr<SystemParams> params);
    SDL_Window* initializeWindow(shared_ptr<SystemParams> params);

    void addButton(shared_ptr<Button> button);
    void addObject(shared_ptr<FSGLObject> object);
    void removeObject(shared_ptr<FSGLObject> object);
	void removeButton(shared_ptr<Button> button);
    void removeAllObjects();

    shared_ptr<FSGLObject> getObjectWithID(string id);
    shared_ptr<Button> getButtonWithUUID(string id);

    void render();
    void stop();

    shared_ptr<FSGLCamera> camera();
    shared_ptr<Screenshot> takeScreenshot();

	void rendererDidTapButton(shared_ptr<Renderer> renderer, shared_ptr<Button> button);

private:
    map<string, shared_ptr<FSGLObject>> idObjectMap;
    map<string, shared_ptr<Button>> uuidButtonsMap;
    shared_ptr<Renderer> renderer;

};

#endif /* FSEOGLCORE_H */

