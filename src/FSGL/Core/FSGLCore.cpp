#include "FSGLCore.h"
#include <iostream>
#include <FSGL/Data/Camera/FSGLCamera.h>
#include <FSGL/Renderer/OGLNewAgeRenderer/OGLNewAgeRenderer.h>

FSGLCore::FSGLCore()
{
    renderer = make_shared<OGLNewAgeRenderer>();
}

FSGLCore::~FSGLCore()
{

}

shared_ptr<FSGLCamera> FSGLCore::camera()
{
    return renderer->camera;
}

void FSGLCore::preInitialize()
{
	renderer->delegate = shared_from_this();
    renderer->preInitialize();
}

void FSGLCore::fillParams(shared_ptr<SystemParams> params)
{
    renderer->fillParams(params);
}

void FSGLCore::rendererDidTapButton(shared_ptr<Renderer> , shared_ptr<Button> button)
{
	auto delegateLocked = delegate.lock();
	if (delegateLocked == nullptr) {
		cout << "Can't did tap on button - delegate is null" << endl;
		return;
	}
	delegateLocked->coreDidTapButton(shared_from_this(), button);
}

SDL_Window* FSGLCore::initializeWindow(shared_ptr<SystemParams> params)
{
    return renderer->initializeWindow(params);
}

void FSGLCore::addButton(shared_ptr<Button> button)
{
	uuidButtonsMap[button->uuid] = button;
	renderer->addButton(button);
}

void FSGLCore::addObject(shared_ptr<FSGLObject> object)
{

    if (object.get() == nullptr)
    {
        throw logic_error("Can't add null object");
    }

    auto id = object->id;

    idObjectMap[id] = object;

    renderer->addObject(object);
}

void FSGLCore::removeObject(shared_ptr<FSGLObject> object)
{
	if (object.get() == nullptr) {
        throw logic_error("Can't remove null object");
	}
    auto id = object->id;
    idObjectMap.erase(id);

    renderer->removeObject(object);

    cout << "Remove object with ID: " << id << endl;
}

void FSGLCore::removeButton(shared_ptr<Button> button)
{
	uuidButtonsMap.erase(button->uuid);
	renderer->removeButton(button);
}

void FSGLCore::removeAllObjects()
{

    for (auto iterator : idObjectMap)
    {
	auto object = iterator.second;
	if (object.get() == nullptr) {
		cout << "warning: removeAllObjects - object.get() == nullptr for some reason" << endl;
	}
	else {
	        renderer->removeObject(object);
	}
    }
	renderer->removeAllButtons();

    idObjectMap.clear();
	uuidButtonsMap.clear();
}

shared_ptr<FSGLObject> FSGLCore::getObjectWithID(string id)
{
    return idObjectMap[id];
}

shared_ptr<Button> FSGLCore::getButtonWithUUID(string uuid) {
	return uuidButtonsMap[uuid];
}

void FSGLCore::render()
{
   renderer->render();
}

void FSGLCore::stop()
{
    renderer->stop();
}

shared_ptr<Screenshot> FSGLCore::takeScreenshot() {
    return renderer->takeScreenshot();
}