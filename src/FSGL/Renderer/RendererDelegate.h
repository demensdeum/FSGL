#ifndef FSGLRENDERERDELEGATE_H_
#define FSGLRENDERERDELEGATE_H_

#include <memory>
#include <FSGL/Data/Button/Button.h>

namespace FSGL {
class Renderer;
};

using namespace std;
using namespace FSGL;

namespace FSGL {

class RendererDelegate {

public:
	virtual void rendererDidTapButton(shared_ptr<Renderer> renderer, shared_ptr<Button> button)  = 0;

};
};


#endif