#ifndef FSGL_RENDERER_H_
#define FSGL_RENDERER_H_

#include <memory>
#include <FSGL/Data/SystemParams.h>
#include <FlameSteelCommonTraits/Screenshot.h>
#include <FSGL/Data/Button/Button.h>
#include <FSGL/Renderer/RendererDelegate.h>

using namespace FSGL;
using namespace std;
class FSGLObject;
class FSGLCamera;
class FSEGTIOGenericSystemParams;

struct SDL_Window;

namespace FSGL
{

class Renderer
{

public:
    Renderer();
    virtual ~Renderer();

	weak_ptr<RendererDelegate> delegate;

    virtual void preInitialize();
    virtual void fillParams(shared_ptr<SystemParams> params) = 0;
    virtual SDL_Window* initializeWindow(shared_ptr<SystemParams> params) = 0;

	virtual void addButton(shared_ptr<Button> button) = 0;
	virtual void removeAllButtons() = 0;

    virtual void addObject(shared_ptr<FSGLObject> object) = 0;
    virtual void removeObject(shared_ptr<FSGLObject> object) = 0;
	virtual void removeButton(shared_ptr<Button> button) = 0;

    virtual void render() = 0;

    virtual void stop() = 0;

    shared_ptr<FSGLCamera> camera;

    virtual shared_ptr<Screenshot> takeScreenshot() = 0;

    SDL_Window *window;
};
};

#endif
