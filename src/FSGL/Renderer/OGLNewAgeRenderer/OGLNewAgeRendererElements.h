#ifndef FSGLOGLNEWAGERENDERERELEMENTS_H_
#define FSGLOGLNEWAGERENDERERELEMENTS_H_

#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD == 1

#define GL_GLEXT_PROTOTYPES 1
#include <SDL_opengles2.h>

#else

#include <GL/glew.h>

#endif

#include <memory>

using namespace std;

class FSGLMesh;

namespace FSGL
{

class OGLNewAgeRendererElements
{

public:
    OGLNewAgeRendererElements(shared_ptr<FSGLMesh> mesh);
    ~OGLNewAgeRendererElements();
    GLuint vao, vbo, indexBuffer;
    GLsizei    indicesCount = 0;
    GLsizeiptr verticesBufferSize, indicesBufferSize;
    GLuint textureBinding = 0;

    void performPreRender();
    void performPostRender();

private:
    shared_ptr<FSGLMesh> mesh;

    void generateBuffers();
    void deleteBuffers();
    void bind();
    void fillTexture();

};

};

#endif
