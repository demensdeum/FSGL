#include "OGLNewAgeRendererElements.h"
#include <FSGL/Data/Mesh/FSGLMesh.h>
#include <FlameSteelCore/FSCUtils.h>

#include <iostream>

#define FSGLOGLNEWAGERENDERER_CPU_BASED_VERTEX_MODS_ENABLED 0

using namespace FSGL;
using namespace FlameSteelCore::Utils;

OGLNewAgeRendererElements::OGLNewAgeRendererElements(shared_ptr<FSGLMesh> mesh)
{
    this->mesh = mesh;

#if FSGLOGLNEWAGERENDERER_CPU_BASED_VERTEX_MODS_ENABLED

#else
    generateBuffers();
#endif

}

void OGLNewAgeRendererElements::generateBuffers() {

    glGenBuffers(1, &vbo);
#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD != 1
    glGenVertexArrays(1, &vao);
#else
    glGenVertexArraysOES(1, &vao);
#endif
    glGenBuffers(1, &indexBuffer);

    verticesBufferSize = mesh->glVerticesBufferSize;
    indicesBufferSize = mesh->glIndicesBufferSize;
    indicesCount = mesh->glIndicesCount;

    glGenTextures(1, &textureBinding);

#if FSGLOGLNEWAGERENDERER_CPU_BASED_VERTEX_MODS_ENABLED
    bind();
#else
    performPreRender();
#endif

    glBufferData(GL_ARRAY_BUFFER, verticesBufferSize, mesh->glVertices, GL_STATIC_DRAW);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBufferSize, mesh->glIndices, GL_STATIC_DRAW);

    fillTexture();
}

void OGLNewAgeRendererElements::performPreRender()
{
    //cout << "performPreRender" << endl;

#if FSGLOGLNEWAGERENDERER_CPU_BASED_VERTEX_MODS_ENABLED
    mesh->simulateVertexShader();
    generateBuffers();
#else
    bind();
#endif

    if (mesh->material == nullptr)
    {
        throwRuntimeException(string("material is null, crashing"));
    }

    if (mesh->material->needsUpdate)
    {
        fillTexture();
        mesh->material->needsUpdate = false;
    }

}

void OGLNewAgeRendererElements::performPostRender() {

    //cout << "performPostRender" << endl;

#if FSGLOGLNEWAGERENDERER_CPU_BASED_VERTEX_MODS_ENABLED
    deleteBuffers();
#endif
}

void OGLNewAgeRendererElements::fillTexture()
{

    auto surface = mesh->material->surface;

    if (surface == nullptr) {
        throwRuntimeException(string("material surface is null, crashing"));
    }

    auto palleteMode = GL_RGBA;

    glTexImage2D(GL_TEXTURE_2D, 0, palleteMode, surface->w, surface->h, 0, palleteMode, GL_UNSIGNED_BYTE, surface->pixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glGenerateMipmap(GL_TEXTURE_2D);

}

void OGLNewAgeRendererElements::bind()
{
#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD != 1
    glBindVertexArray(vao);
#else
    glBindVertexArrayOES(vao);
#endif
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
    glBindTexture(GL_TEXTURE_2D, textureBinding);

}

void OGLNewAgeRendererElements::deleteBuffers() {

#if CUBE_ART_PROJECT_ANDROID_NDK_BUILD != 1
    glDeleteVertexArrays(1, &vao);
#else
    glDeleteVertexArraysOES(1, &vao);
#endif
    glDeleteBuffers(1, &vbo);
    glDeleteBuffers(1, &indexBuffer);
    glDeleteTextures(1, &textureBinding);
}

OGLNewAgeRendererElements::~OGLNewAgeRendererElements()
{
#if FSGLOGLNEWAGERENDERER_CPU_BASED_VERTEX_MODS_ENABLED

#else
    deleteBuffers();
#endif

}
