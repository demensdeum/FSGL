/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   FSEOGLController.h
 * Author: demensdeum
 *
 * Created on July 8, 2017, 10:37 AM
 */

#ifndef FSEOGLCONTROLLER_H
#define FSEOGLCONTROLLER_H

#include <FSGL/Controller/FSGLControllerDelegate.h>
#include <FlameSteelCommonTraits/Screenshot.h>
#include <FlameSteelCommonTraits/IOSystemParams.h>
#include <FSGL/Data/Button/Button.h>
#include <FSGL/Data/Model/FSGLModel.h>
#include <FSGL/Data/Camera/FSGLCamera.h>
#include <FSGL/Core/FSGLCore.h>
#include <memory>

using namespace FSGL;
using namespace std;

class FSGLController: public enable_shared_from_this<FSGLController>, public FSGLCoreDelegate
{

public:
    FSGLController();
    virtual ~FSGLController();

	weak_ptr<FSGLControllerDelegate> delegate;

    void preInitialize();
    void fillParams(shared_ptr<SystemParams> params);
    SDL_Window* initializeWindow(shared_ptr<SystemParams> params);

    void addObject(shared_ptr<FSGLObject> object);
	void addButton(shared_ptr<Button> button);

    shared_ptr<FSGLObject> getObjectWithID(string id);
	shared_ptr<Button> getButtonWithUUID(string uuid);

    void removeAllObjects();

    void removeObject(shared_ptr<FSGLObject> object);
	void removeButton(shared_ptr<Button> button);

    void render();

    void stop();

    void setCameraX(float x);
    void setCameraY(float y);
    void setCameraZ(float z);

    void setCameraRotationX(float x);
    void setCameraRotationY(float y);
    void setCameraRotationZ(float z);

    shared_ptr<Screenshot> takeScreenshot();

	void coreDidTapButton(shared_ptr<FSGLCore> core, shared_ptr<Button> button);

private:
    shared_ptr<FSGLCore> core;
};

#endif /* FSEOGLCONTROLLER_H */

