#ifndef FSGLCONTROLLERDELEGATE_H
#define FSGLCONTROLLERDELEGATE_H

#include <memory>
#include <FSGL/Data/Button/Button.h>

using namespace std;

class FSGLController;

namespace FSGL {

class FSGLControllerDelegate {

public:
	virtual void fsglControllerDidTapButton(shared_ptr<FSGLController> core, shared_ptr<Button> button) = 0;

};
};

#endif