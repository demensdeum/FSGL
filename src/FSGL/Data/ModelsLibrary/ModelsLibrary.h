#ifndef FSGLMODELSLIBRARY_H_
#define FSGLMODELSLIBRARY_H_

#include <memory>
#include <FSGL/Data/Model/FSGLModel.h>
#include <FlameSteelCore/SharedNotNullPointer.h>

using namespace Shortcuts;
using namespace std;

namespace FSGL {

class ModelsLibrary {

public:
	ModelsLibrary() {}

	void saveModelByKey(NotNull<FSGLModel> model, string key);
	shared_ptr<FSGLModel> getModelByKey(string key);

private:
	map<string, NotNull<FSGLModel>> cache;


};

};

#endif