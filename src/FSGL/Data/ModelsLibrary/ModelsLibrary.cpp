#include "ModelsLibrary.h"


void ModelsLibrary::saveModelByKey(NotNull<FSGLModel> model, string key) {
	cache[key] = model.sharedPointer();
};

shared_ptr<FSGLModel> ModelsLibrary::getModelByKey(string key) {

	if (cache.find(key) != cache.end()) {
		return cache[key].sharedPointer();
	}

	return shared_ptr<FSGLModel>();

};