/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   FSGLMesh.cpp
 * Author: demensdeum
 *
 * Created on August 1, 2017, 11:02 PM
 */

#include <glm/detail/type_vec.hpp>

#include "FSGLMesh.h"

#include <FlameSteelCore/FSCUtils.h>
#include "../Object/FSGLObject.h"
#include <FSGL/Data/Bone/FSGLBone.h>
#include <FSGL/Data/AnimationTransformationMatrix/AnimationTransformationMatrix.h>

#include <FSGL/Data/Animator/Animator.h>
#include <FSGL/Data/Matrix/FSGLMatrix.h>

#include <iostream>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace FlameSteelGraphicsLayer;
using namespace FlameSteelCore::Utils;

FSGLMesh::FSGLMesh()
{
}

int FSGLMesh::glVertexCount()
{
    vector<string> vertexFieldsDescription =
    {
        "x",
        "y",
        "z",
        "u",
        "v",
        "bone0index",
        "bone0weight",
        "bone1index",
        "bone1weight",
        "bone2index",
        "bone2weight",
    };
    return int(vertexFieldsDescription.size());
}

GLsizei FSGLMesh::glVertexSize()
{
    return sizeof(GLfloat) * glVertexCount();
}

void FSGLMesh::simulatedVertexShaderFunction(shared_ptr<FSGLVertex> vertex, vector<shared_ptr<FSGLMatrix> > bones)
{
    if (parentModel.get() == nullptr) {
        throwRuntimeException(string("simulatedVertexShaderFunction parentModel is null"));
    }
    if (parentModel->currentAnimation != nullptr && parentModel->currentAnimation->isEnded() == false) {
        //cout << "simulatedVertexShaderFunction currentAnimation is not null" << endl;
    }
    else {
        return;
    }

    cout << "---" << endl;

    if (parentModel->currentAnimation->isEnded() == false) {
        if (bones.size() < 1) {
            throwRuntimeException("No transform matrices from animator... exception");
        }
        auto boneIndex = vertex->indexForAnimationMatrixAtShaderIndex(0);
        cout << "bone index" << boneIndex << endl;
        bones[boneIndex]->multiplyWithVector(vertex->position);
    }

}

void FSGLMesh::simulateVertexShader()
{
    vector<shared_ptr<FSGLVertex> > originalVerticesObjects;

    auto bones = Animator::transformMatricesForMesh(shared_from_this());

    for (auto vertex : verticesObjects) {
        auto vertexCopy = vertex->copy();
        originalVerticesObjects.push_back(vertexCopy);
        simulatedVertexShaderFunction(vertex, bones);
    }
    updateGlData();

    verticesObjects = originalVerticesObjects;
}

void FSGLMesh::populateAnimationTransformationMatrices()
{

    cout << "FSGLMesh::populateAnimationTransformationMatrices()" << endl;

    animationTransformationMatrices.clear();

    auto animationTransformationMatrix = make_shared<AnimationTransformationMatrix>();
    animationTransformationMatrix->index = 0;
    animationTransformationMatrix->name = make_shared<string>("Unit Matrix");
    animationTransformationMatrix->matrix = make_shared<FSGLMatrix>();

    animationTransformationMatrices.push_back(animationTransformationMatrix);

    cout << "bones.size = " << bones.size() << endl;

    for (auto bone : bones)
    {
        if (bone->name.get() == nullptr)
        {
            throw runtime_error("Bone name is null");
        }
        auto boneName = bone->name->c_str();

        auto animationTransformationMatrix = make_shared<AnimationTransformationMatrix>();
        animationTransformationMatrix->index = animationTransformationMatrices.size();
        animationTransformationMatrix->name = make_shared<string>(boneName);
        animationTransformationMatrix->matrix = make_shared<FSGLMatrix>();
        animationTransformationMatrix->vertexWeights = bone->vertexWeights;

        for (auto vertexWeight : animationTransformationMatrix->vertexWeights)
        {
            auto vertexID = vertexWeight->vertexID;
            auto weight = vertexWeight->weight;
            auto vertexObject = this->verticesObjects[vertexID];
            vertexObject->setWeigthForMatrixAtIndex(weight, animationTransformationMatrix->index);
            cout << "Vertex object setWeightForMatrixAtIndex" << endl;
        }

        cout << "Added bone with index: " << animationTransformationMatrix->index << endl;

        animationTransformationMatrices.push_back(animationTransformationMatrix);
    }

    cout << "Populated animation transformation matrices vector, size: " << animationTransformationMatrices.size() << endl;
}

shared_ptr<AnimationTransformationMatrix> FSGLMesh::animationTransformationMatrixWithName(shared_ptr<string> name)
{

    for (auto animationTransformationMatrix : animationTransformationMatrices)
    {
        if (*(animationTransformationMatrix->name.get()) == *(name.get()) )
        {
            return animationTransformationMatrix;
        }
    }

    string errorName = "No such animation transformation matrix with name ";
    errorName += *(name.get());

    throw runtime_error(errorName);

    return shared_ptr<AnimationTransformationMatrix>();
}

void FSGLMesh::freeGlData()
{
    if (glIndices != NULL)
    {

        delete glIndices;
        glIndices = nullptr;

    }

    if (glVertices != NULL)
    {

        delete glVertices;
        glVertices = nullptr;

    }
}

void FSGLMesh::updateGlData()
{
    freeGlData();

    glVertices = new GLfloat[verticesObjects.size() * glVertexCount()];

    //cout << "verticesObjects.size() " << verticesObjects.size() << endl;

    auto count = glVertexCount();

    for (unsigned int i = 0; i < verticesObjects.size() * count; i += count)
    {

        auto vertexObject = verticesObjects[i / count];

        // x, y, z, u, v, bone0index, bone0weight, bone1index, bone1weight, bone2index, bone2weight, bone3index, bone3weight

        // x, y, z

        glVertices[i] = vertexObject->position->x;
        glVertices[i + 1] = vertexObject->position->y;
        glVertices[i + 2] = vertexObject->position->z;

        // UV

        glVertices[i + 3] = vertexObject->uvTextureCoordinates->u;
        glVertices[i + 4] = vertexObject->uvTextureCoordinates->v;

        // animation matrices info
        // use one bone at index zero by default

        glVertices[i + 5] = vertexObject->indexForAnimationMatrixAtShaderIndex(0);
        glVertices[i + 6] = vertexObject->vertexWeightAtShaderIndex(0);

        glVertices[i + 7] = vertexObject->indexForAnimationMatrixAtShaderIndex(1);
        glVertices[i + 8] = vertexObject->vertexWeightAtShaderIndex(1);

        glVertices[i + 9] = vertexObject->indexForAnimationMatrixAtShaderIndex(2);
        glVertices[i + 10] = vertexObject->vertexWeightAtShaderIndex(2);

    }

    glIndices = new GLushort[indices.size() * sizeof (GLushort)];

    for (unsigned int i = 0; i < indices.size(); i++)
    {

        glIndices[i] = indices[i];

    }

    glVerticesBufferSize = verticesObjects.size() * count * sizeof (GLfloat);
    glIndicesBufferSize = indices.size() * sizeof (GLushort);
    glIndicesCount = indices.size();

    isGLDataPrepared = true;

}

FSGLMesh::~FSGLMesh()
{

    freeGlData();

}
