/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   FSGLNodeAnimation.cpp
 * Author: demensdeum
 *
 * Created on November 5, 2017, 2:32 PM
 */

#include "FSGLNodeAnimation.h"

#define GLM_ENABLE_EXPERIMENTAL 1

#include <sstream>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <FSGL/Data/Node/FSGLNode.h>
#include <FSGL/Data/Animation/FSGLAnimation.h>

#include <iostream>

FSGLNodeAnimation::FSGLNodeAnimation()
{

}

FSGLNodeAnimation::FSGLNodeAnimation(const FSGLNodeAnimation& )
{
}

FSGLNodeAnimation::~FSGLNodeAnimation()
{
}

shared_ptr<FSGLVector> FSGLNodeAnimation::positionVectorFor(shared_ptr<FSGLAnimation> currentAnimation)
{

    auto ticks = currentAnimation->ticks();

    auto previousPosition = shared_ptr<FSGLVectorKeyframe>();
    auto nextPosition = shared_ptr<FSGLVectorKeyframe>();

    cout << "Positions size: " << positions.size() << endl;
    cout << "---" << endl;

    for (size_t i = 0; i < positions.size(); i++)
    {
        auto position = positions[i];
        auto time = position->time;

        cout << "position index " << i << endl;
        cout << "position->time " << time << endl;
        cout << "ticks " << ticks << endl;

        if (time < ticks)
        {
            cout << "previous position set" << endl;
            previousPosition = position;
        }
        else
        {
            cout << "nextPosition set, break" << endl;
            nextPosition = position;
            break;
        }
    }

    cout << "---" << endl;

    if (previousPosition.get() != nullptr && nextPosition.get() != nullptr)
    {
        cout << "Found all data for interpolation" << endl;

        auto outputVector = make_shared<FSGLVector>();

        auto previousPositionVector = previousPosition->vector;
        auto nextPositionVector = nextPosition->vector;

        auto factor = float(ticks) / float(nextPosition->time);

        cout << "factor: " << factor << endl;

        auto xDiff = nextPositionVector->x - previousPositionVector->x;
        auto yDiff = nextPositionVector->y - previousPositionVector->y;
        auto zDiff = nextPositionVector->z - previousPositionVector->z;

        outputVector->x = previousPositionVector->x + (xDiff * factor);
        outputVector->y = previousPositionVector->y + (yDiff * factor);
        outputVector->z = previousPositionVector->z + (zDiff * factor);

        return outputVector;
    }
    else
    {
        cout << "Can't find all data for position interpolation" << endl;
    }

    return positions[0]->vector;

}

shared_ptr<FSGLQuaternion> FSGLNodeAnimation::rotationFor(shared_ptr<FSGLAnimation> currentAnimation)
{

    auto ticks = currentAnimation->ticks();

    auto previousKeyframe = shared_ptr<FSGLQuaternionKeyframe>();
    auto nextKeyframe = shared_ptr<FSGLQuaternionKeyframe>();

    cout << "Rotations size: " << rotations.size() << endl;
    cout << "---" << endl;

    for (size_t i = 0; i < rotations.size(); i++)
    {
        auto rotation = rotations[i];
        auto time = rotation->time;

        cout << " index " << i << endl;
        cout << "rotation->time " << time << endl;
        cout << "ticks " << ticks << endl;

        if (time < ticks)
        {
            cout << "previous rotation set" << endl;
            previousKeyframe = rotation;
        }
        else
        {
            cout << "next rotation set, break" << endl;
            nextKeyframe = rotation;
            break;
        }
    }

    cout << "---" << endl;

    if (previousKeyframe.get() != nullptr && nextKeyframe.get() != nullptr)
    {
        cout << "Found all data for interpolation" << endl;

        auto outputQuaternion = make_shared<FSGLQuaternion>();

        auto previousKeyframeQuaternion = previousKeyframe->quaternion;
        auto nextKeyframeQuaternion = nextKeyframe->quaternion;

        auto factor = float(ticks) / float(nextKeyframe->time);

        cout << "factor: " << factor << endl;

        auto wDiff = nextKeyframeQuaternion->quaternion.w - previousKeyframeQuaternion->quaternion.w;
        auto xDiff = nextKeyframeQuaternion->quaternion.x - previousKeyframeQuaternion->quaternion.x;
        auto yDiff = nextKeyframeQuaternion->quaternion.y - previousKeyframeQuaternion->quaternion.y;
        auto zDiff = nextKeyframeQuaternion->quaternion.z - previousKeyframeQuaternion->quaternion.z;

        auto w = previousKeyframeQuaternion->quaternion.w + (wDiff * factor);
        auto x = previousKeyframeQuaternion->quaternion.x + (xDiff * factor);
        auto y = previousKeyframeQuaternion->quaternion.y + (yDiff * factor);
        auto z = previousKeyframeQuaternion->quaternion.z + (zDiff * factor);

        outputQuaternion->quaternion = glm::quat(w, x, y, z);

        return outputQuaternion;
    }
    else
    {
        cout << "Can't find all data for rotation interpolation" << endl;
    }

    return rotations[0]->quaternion;

}

shared_ptr<FSGLVector> FSGLNodeAnimation::scalingFor(shared_ptr<FSGLAnimation> currentAnimation)
{

    auto ticks = currentAnimation->ticks();

    auto previousScaling = shared_ptr<FSGLVectorKeyframe>();
    auto nextScaling = shared_ptr<FSGLVectorKeyframe>();

    cout << "Scaling size: " << scalings.size() << endl;
    cout << "---" << endl;

    for (size_t i = 0; i < scalings.size(); i++)
    {
        auto scaling = scalings[i];
        auto time = scaling->time;

        cout << "scaling index " << i << endl;
        cout << "scaling->time " << time << endl;
        cout << "ticks " << ticks << endl;

        if (time < ticks)
        {
            cout << "previous scaling set" << endl;
            previousScaling = scaling;
        }
        else
        {
            cout << "nextPosition set, break" << endl;
            nextScaling = scaling;
            break;
        }
    }

    cout << "---" << endl;

    if (previousScaling.get() != nullptr && nextScaling.get() != nullptr)
    {
        cout << "Found all data for interpolation" << endl;

        auto outputVector = make_shared<FSGLVector>();

        auto previousScalingVector = previousScaling->vector;
        auto nextScalingVector = nextScaling->vector;

        auto factor = float(ticks) / float(nextScaling->time);

        cout << "factor: " << factor << endl;

        auto xDiff = nextScalingVector->x - previousScalingVector->x;
        auto yDiff = nextScalingVector->y - previousScalingVector->y;
        auto zDiff = nextScalingVector->z - previousScalingVector->z;

        outputVector->x = previousScalingVector->x + (xDiff * factor);
        outputVector->y = previousScalingVector->y + (yDiff * factor);
        outputVector->z = previousScalingVector->z + (zDiff * factor);

        return outputVector;
    }
    else
    {
        cout << "Can't find all data for scaling interpolation" << endl;
    }

    return scalings[0]->vector;

}

shared_ptr<string> FSGLNodeAnimation::serializeIntoString(int identation)
{
    stringstream serializedData;

    for (auto i = 0; i < identation; i++)
    {
        serializedData << "\t";
    }

    serializedData << "Node Animation - " << *name << endl;

    if (positions.size() > 0)
    {

        for (auto i = 0; i < identation; i++)
        {
            serializedData << "\t";
        }

        serializedData << "Position Keyframes" << endl;
        for (auto position : positions)
        {
            serializedData << position->serializeIntoString(identation + 1)->c_str() << endl;
        }

        for (auto i = 0; i < identation; i++)
        {
            serializedData << "\t";
        }
        serializedData << "Scaling Keyframes" << endl;
        for (auto scaling : scalings)
        {
            serializedData << scaling->serializeIntoString(identation + 1)->c_str() << endl;
        }


        for (auto i = 0; i < identation; i++)
        {
            serializedData << "\t";
        }
        serializedData << "Rotation Keyframes" << endl;
        for (auto rotation : rotations)
        {
            serializedData << rotation->serializeIntoString(identation + 1)->c_str() << endl;
        }
    }

    auto stringContainer = make_shared<string>(serializedData.str());
    return stringContainer;
}
