#include "FSGLModelLoaderFSGL.h"

#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

shared_ptr<FSGLModel> FSGLModelLoaderFSGL::loadModel(shared_ptr<string> modelPath, shared_ptr<MaterialLibrary> materialLibrary, NotNull<ModelsLibrary> modelsLibrary)
{
	string key = *modelPath.get();

	auto cachedModel = modelsLibrary->getModelByKey(key);
	if (cachedModel.get() != nullptr) {
		cout << "CACHED MODEL: " << key << endl;
		return cachedModel;
	}

    cout << "Load model at path: " << *modelPath << endl;
    std::ifstream t(modelPath->c_str());
    if (!t) {
        throw std::system_error(errno, std::system_category(), "failed to open " + *modelPath);
    }
    std::stringstream buffer;
    buffer << t.rdbuf();

    auto serializedModelString = make_shared<string>(buffer.str());

    auto model = make_shared<FSGLModel>()->deserializeFromString(serializedModelString, materialLibrary);

	modelsLibrary->saveModelByKey(model, key);

    return static_pointer_cast<FSGLModel>(model);

}