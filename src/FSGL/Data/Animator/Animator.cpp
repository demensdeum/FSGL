#include "Animator.h"
#include <iostream>
#include <FSGL/Data/Matrix/FSGLMatrix.h>
#include <FSGL/Data/Mesh/FSGLMesh.h>
#include <FSGL/Data/Model/FSGLModel.h>
#include <FSGL/Data/Bone/FSGLBone.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/glm.hpp>
#include <FlameSteelCore/FSCUtils.h>

#define GLM_ENABLE_EXPERIMENTAL 1

using namespace FlameSteelCore::Utils;
using namespace std;
using namespace FlameSteelGraphicsLayer;

vector<shared_ptr<FSGLMatrix> > Animator::transformMatricesForMesh(shared_ptr<FSGLMesh> mesh) {

    // pose bones per mesh, and output

    auto unitMatrix = make_shared<FSGLMatrix>();
    vector<shared_ptr<FSGLMatrix> > matrices;
    matrices.push_back(unitMatrix);

    for (auto bone : mesh->bones) {

        auto transformMatrix = make_shared<FSGLMatrix>();
        matrices.push_back(transformMatrix);

        auto boneName = *bone->name;
        auto boneNode = mesh->parentModel->findNode(boneName);
        if (boneNode.get() == nullptr) {
            string errorText = "Model does not contain node with same name as bone: ";
            errorText += boneName;
            throwRuntimeException(errorText);
        }

        vector<string> nodesNames;
        nodesNames.push_back(*boneNode->name);
        auto parentNode = boneNode->parent;
        while(parentNode.get() != nullptr) {
            string nodeName = *parentNode->name;
            nodesNames.push_back(nodeName);
            parentNode = parentNode->parent;
        }

        for (auto nodeName : nodesNames) {
            cout << nodeName << endl;
            auto currentAnimation = mesh->parentModel->currentAnimation;
            if (currentAnimation.get() == nullptr) {
                break;
            }
            for (auto nodeAnimation : currentAnimation->nodeAnimations) {
                if (*nodeAnimation->name == nodeName) {
                    cout << "applying animation for nodeName: " << nodeName << endl;
                    auto positionVector = nodeAnimation->positionVectorFor(currentAnimation);
                    cout << "Position x: " << positionVector->x << "  " << endl;
                    cout << "Position y: " << positionVector->y << "  " << endl;
                    cout << "Position z: " << positionVector->z << "  " << endl;
                    transformMatrix->matrix = glm::translate(transformMatrix->matrix, glm::vec3(positionVector->x, positionVector->y, positionVector->z));

                    auto rotationQuaternion = nodeAnimation->rotationFor(currentAnimation);
                    cout << "Rotation w: " << rotationQuaternion->quaternion.w << "  " << endl;
                    cout << "Rotation x: " << rotationQuaternion->quaternion.x << "  " << endl;
                    cout << "Rotation y: " << rotationQuaternion->quaternion.y << "  " << endl;
                    cout << "Rotation z: " << rotationQuaternion->quaternion.z << "  " << endl;

                    auto scalingVector = nodeAnimation->scalingFor(currentAnimation);
                    cout << "Scale x: " << scalingVector->x << "  " << endl;
                    cout << "Scale y: " << scalingVector->y << "  " << endl;
                    cout << "Scale z: " << scalingVector->z << "  " << endl;

                    transformMatrix->matrix = transformMatrix->matrix * glm::toMat4(rotationQuaternion->quaternion);
                    transformMatrix->matrix = glm::scale(transformMatrix->matrix, glm::vec3(scalingVector->x, scalingVector->y, scalingVector->z));
                }
            }
        }


    }

    return matrices;

}
