#ifndef FSGLANIMATOR_H_
#define FSGLANIMATOR_H_

#include <vector>
#include <memory>

class FSGLMatrix;
class FSGLMesh;

using namespace std;

namespace FlameSteelGraphicsLayer {

class Animator {

public:
    static vector<shared_ptr<FSGLMatrix> > transformMatricesForMesh(shared_ptr<FSGLMesh> mesh);

};

};


#endif