#ifndef SPACEJAGUARACTIONRPGLINUXFSGLSYSTEMPARAMS_H_
#define SPACEJAGUARACTIONRPGLINUXFSGLSYSTEMPARAMS_H_

#include <FlameSteelCommonTraits/IOSystemParams.h>

namespace FSGL {

class SystemParams: public IOSystemParams {

public:
    SystemParams() : IOSystemParams() {};

    bool oldSchoolVibeEnabled = false;
    bool raiseExecutionWhenModelLoadingTooLong = false;

    virtual ~SystemParams() {};

};

};

#endif